# correlation-id-boot-sl4j

An implementation of correlation ids for Spring Boot and SL4J with minimal dependencies.

The initial version was copied from Peterst's JDriven post ["Correlate your services logging with Spring Boot"](https://blog.jdriven.com/2017/04/correlate-services-logging-spring-boot/)
and enhanced for [my use case](http://backlogprint.agilestationery.co.uk) based on experiences with similar solutions.

## How correlation ids are assigned

This filter retains Peterst's feature to supply a correlation id on an inbound HTTP request by adding a header named 
`CID`. If this is not supplied the correlation id is generated randomly from an alphabet of URL safe characters. By 
default, 5 characters are chosen for each random correlation id.

## Overriding default behaviour

Define your own Spring bean of type `CorrelationIdFilter` and supply a `CidGenerator`. 

`CidGenerator` is an interface ands the default implementation is `RandomCidGenerator`.


## Set Up

Just add the Maven dependency and add the `CID` MDC value to your logging.

The `CorrelationIdFilter` is auto-configured and will be wrapped around the dispatcher servlet.

For Maven dependency information please refer to the [JitPack page for this module](https://jitpack.io/#org.bitbucket.agilestationery/correlation-id-boot-sl4j).

For log set up documentation relevant to Spring Boot, try [starting with LogBack](https://logback.qos.ch/manual/layouts.html#mdc)

### Output the correlation Id in logs

In standard Spring Boot logging set ups, you just need to add the following snippet to your log pattern:
 
```
%X{CID}
```

All log events related (by thread) to an HTTP request will include a 5 character correlation id by default. To include a 
placeholder `---` when the MDC is not present, add the following instead:

```
%X{CID:----}
```

A non-invasive method to add the CID to log output is to include it alongside the level. To do so, simply add this 
property to your `application.properties` file:
 
```
logging.pattern.level=%X{CID:----} %5p
```

If you are using JSON formatters ensure they are set to include your whole MDC and do not set the property above.

### Extra log events

The span of each correlation id is marked with "Handling" and "Cleared" log event.

These events are logged by the logger / log category `org.bitbucket.agilestationery.correlation.CorrelationIdFilter` at 
`INFO` level.

The following name/value pairs are present in these events:

cid=`<correlation id>`

elapsedMs=`XX`

For `elapsedMs` the value is time elapsed while the MDC `CID` value was set, in milliseconds. This offers a convenient 
way to record how long Spring and your application code are taking to perform HTTP requests. 
The data will be persisted alongside your logs allowing re-use of log analysis solutions. 

# Related Tools

Log output can be analysed in tools such as Splunk or LogEntries. 

I recommend you also consider metering key operations with a metrics framework such as DropWizard 
Metrics.