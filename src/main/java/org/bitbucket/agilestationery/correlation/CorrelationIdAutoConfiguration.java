package org.bitbucket.agilestationery.correlation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = CorrelationIdAutoConfiguration.class)
public class CorrelationIdAutoConfiguration {

	@Bean
	@ConditionalOnMissingBean
	public CorrelationIdFilter correlationIdFilter() {
		return new CorrelationIdFilter();
	}

}
