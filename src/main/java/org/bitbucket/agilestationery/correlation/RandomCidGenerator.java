package org.bitbucket.agilestationery.correlation;

import java.util.Random;

import static java.lang.String.valueOf;
import static java.util.stream.Collectors.joining;

public class RandomCidGenerator implements CidGenerator {

	private final static Random RANDOM = new Random();

	public static final char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

	private int length;

	public RandomCidGenerator(int length) {
		if(length<1) {
			throw new IllegalArgumentException("length is too small: " + length);
		}
		this.length = length;
	}

	@Override
	public String generateCid() {
		return RANDOM.ints(length, 0, ALPHABET.length)
				.mapToObj((i) -> valueOf(ALPHABET[i]))
				.collect(joining());
	}
}
