package org.bitbucket.agilestationery.correlation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import java.io.IOException;


@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorrelationIdFilter implements Filter {

	static final String MDC_KEY = "CID";

	private static Logger LOGGER = LoggerFactory.getLogger(CorrelationIdFilter.class);

	private CidGenerator cidGenerator;

	public CorrelationIdFilter() {
		this(new RandomCidGenerator(5));
	}

	public CorrelationIdFilter(CidGenerator cidGenerator) {
		this.cidGenerator = cidGenerator;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		long startTime = System.currentTimeMillis();
		String chosenCid = null;
		if (req instanceof HttpServletRequest) {
			HttpServletRequest request = (HttpServletRequest) req;
			chosenCid = request.getHeader(CorrelationIdFilter.MDC_KEY);
		}

		if(chosenCid==null) {
			chosenCid = cidGenerator.generateCid();
		}

		// add cid to the MDC
		MDC.put(MDC_KEY, chosenCid);

		LOGGER.info("Handling cid={}", chosenCid);

		try {
			// call filter(s) upstream for the real processing of the request
			chain.doFilter(req, res);
		} finally {

			// We want to know that the span of the CID is done, and how long it took
			long elapsedMillis = System.currentTimeMillis() - startTime;

			// log this before removing the CID. Makes more sense when consumed.
			LOGGER.info("Cleared cid={}, elapsedMs={}", chosenCid, elapsedMillis);

			// it's important to always clean the cid from the MDC,
			// this Thread goes to the pool but it's loglines would still contain the cid.
			MDC.remove(MDC_KEY);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {


	}


	@Override
	public void destroy() {

	}

}
