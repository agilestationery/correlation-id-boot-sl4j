package org.bitbucket.agilestationery.correlation;

public interface CidGenerator {
	String generateCid();
}
