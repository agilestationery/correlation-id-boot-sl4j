package org.bitbucket.agilestationery.correlation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RandomCidGeneratorTest {


	@Test
	void shouldRejectZeroLength() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new RandomCidGenerator(0)
			);
	}

	@Test
	void shouldRejectNegativeLength() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new RandomCidGenerator(-1)
		);
	}

	@Test
	void shouldGenerateLongCid() {
		RandomCidGenerator generator = new RandomCidGenerator(65536);
		String longCid = generator.generateCid();
		assertEquals(65536, longCid.length());
	}

	@Test
	void shouldGenerateShortCid() {
		RandomCidGenerator generator = new RandomCidGenerator(5);
		String longCid = generator.generateCid();
		assertEquals(5, longCid.length());
	}

	@Test
	void shouldUsuallyGenerateUniqueCids() {
		RandomCidGenerator generator = new RandomCidGenerator(5);
		String firstCid = generator.generateCid();
		String secondCid = generator.generateCid();
		assertFalse(firstCid.equals(secondCid));
	}

}
