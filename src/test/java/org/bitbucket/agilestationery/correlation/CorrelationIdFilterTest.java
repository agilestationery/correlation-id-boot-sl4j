package org.bitbucket.agilestationery.correlation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.MDC;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.bitbucket.agilestationery.correlation.CorrelationIdFilter.MDC_KEY;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class CorrelationIdFilterTest {

	public static final String TEST_CID = "TEST_CID";
	private FilterChain chain;
	private HttpServletRequest request;
	private HttpServletResponse response;

	@BeforeEach
	void setUpMocks() {
		chain = mock(FilterChain.class);
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
	}


	@Test
	void shouldClearMdcOnException() throws IOException, ServletException {
		doThrow(new RuntimeException("simulated exception"))
				.when(chain).doFilter(any(), any());

		assertThrows(RuntimeException.class,this::runWithDefaultMocks);

		assertNull(MDC.get(MDC_KEY));

	}

	@Test
	void shouldClearMdcWhenThereIsNoException() throws IOException, ServletException {
		runWithDefaultMocks();
		assertNull(MDC.get(MDC_KEY));
	}

	private void runWithDefaultMocks() throws IOException, ServletException {
		CorrelationIdFilter filter = new CorrelationIdFilter();
		filter.doFilter(request, response, chain);
	}

	@Test
	void shouldSetRequestedCIDWhenProvided() throws IOException, ServletException {
		when(request.getHeader(any())).thenReturn(TEST_CID);

		MdcCapturingAnswer captureMdc = prepareToCaptureMdc();

		runWithDefaultMocks();

		assertEquals(TEST_CID, captureMdc.getPrevailingMdc());

	}



	@Test
	void shouldSetACidWhenNotProvided() throws IOException, ServletException {
		MdcCapturingAnswer captureMdc = prepareToCaptureMdc();

		runWithDefaultMocks();

		assertNotNull(captureMdc.getPrevailingMdc());
	}

	@Test
	void shouldNotUsuallySetSameCidTwice() throws IOException, ServletException {
		MdcCapturingAnswer captureMdc = prepareToCaptureMdc();

		runWithDefaultMocks();

		String firstCid = captureMdc.getPrevailingMdc();

		runWithDefaultMocks();

		String secondCid = captureMdc.getPrevailingMdc();

		// if this EVER fails then uniqueness is probably not good enough, but in theory it still might be okay
		assertNotEquals(firstCid, secondCid, "Generated CID values must usually differ between runs");

	}

	@Test
	void shouldCallTheChain() throws IOException, ServletException {
		runWithDefaultMocks();

		verify(chain).doFilter(any(), any());
	}


	private MdcCapturingAnswer prepareToCaptureMdc() throws IOException, ServletException {
		MdcCapturingAnswer captureMdc = new MdcCapturingAnswer();

		doAnswer(captureMdc).when(chain).doFilter(any(),any());
		return captureMdc;
	}

	private static class MdcCapturingAnswer implements Answer<Void> {
		public String prevailingMdc;

		@Override
		public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
			prevailingMdc =  MDC.get(MDC_KEY);
			return null;
		}

		public String getPrevailingMdc() {
			return prevailingMdc;
		}
	}

}
